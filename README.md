# osu profile twitter handle scraper
A python script which gets twitter handles from multiple osu profile pages.
Literally my first python project which I made while watching some tutorials. You will need a linux pc if you want to run this script unmodified, not sure if this script works on other OS's.
Feel free to contribute to this if you want I guess.

## Used projects
- Selenium
- Mozilla Firefox web driver (geckodriver)

## Getting started
- Install selenium with `pip install selenium`
- Install geckodriver package if you're using Linux. For other OS's download geckodriver from [here](https://github.com/mozilla/geckodriver/releases) and replace `/usr/bin/geckodriver` in the 8th line to the path to your geckodriver executable
- Save osu profile page url's you want to scrape to osu_profiles.txt (in the same folder as the script) and put them into seperate lines
- Run the script (the script will save the twitter profile url's to twitter_links.txt file)

## License
This project is licensed under a GPL v3 license
