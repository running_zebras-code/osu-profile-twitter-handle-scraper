from selenium.webdriver import Firefox
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.service import Service
from selenium.common.exceptions import NoSuchElementException
import selenium
import time
srv=Service("/usr/bin/geckodriver")
driver = webdriver.Firefox(service=srv)
twitterlinks = open("twitter_links.txt", "a")
with open("osu_profiles.txt", "r") as file:
  for line in file:
    i=0
    url = line.rstrip("\n")
    driver.get(url)
    try:
        element = driver.find_element(By.XPATH, value="//a[contains(@href, 'twitter.com')]")  
        print(element.get_attribute("href"))
        twitterlinks.write(element.get_attribute("href")+"\n")
    except selenium.common.exceptions.NoSuchElementException as error:
        print("")
    i=i+1
    time.sleep(3)
pass
twitterlinks.close();
driver.quit()
